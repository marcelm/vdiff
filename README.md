vdiff
=====

A command-line tool that highlights changed words or characters in a diff.

A global alignment algorithm is used to find differences. This is not very efficient, and in fact, `diff` itself uses a better algorithm. Since this tool is still quite useful, I am publishing it here.


License
=======

(This is the MIT license.)

Copyright (c) 2005, 2010, 2014 Marcel Martin <marcel.martin@scilifelab.se>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


Dependencies
============

Python 3.2 or later must be installed.


Running
=======

Either install `vdiff` with:

    python3 setup.py install

Or just run the script `bin/vdiff` directly.


Example output
==============

![Screenshot of git diff|vdiff output](https://bitbucket.org/marcelm/vdiff/raw/master/screenshot.png)
