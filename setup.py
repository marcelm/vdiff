from distutils.core import setup
import sys

if sys.version < '3.2':
	sys.stdout.write("At least Python 3.2 is required.\n")
	sys.exit(1)

setup(
	name = 'vdiff',
	version = '0.2',
	author = 'Marcel Martin',
	author_email = 'marcel.martin@scilifelab.se',
	url = 'https://bitbucket.org/marcelm/vdiff',
	description = 'Colorize diff output on the command line',
	license = 'MIT',
	packages = ['vdiff'],
	scripts = ['bin/vdiff'],
	classifiers = [
		"Development Status :: 4 - Beta",
		"Environment :: Console",
		"License :: OSI Approved :: MIT License",
		"Natural Language :: English",
		"Programming Language :: Python :: 3.2",
	]
)
