#!/usr/bin/env python3
"""
Pipe a unified diff (diff -u) through this program to get a
word-based or character-based visualization of differences.

Examples:

diff -u file1 file2 | vdiff

git diff | vdiff
"""

"""
TODO
* this is broken in Python3 with unicode characters
* make word-based comparison work again (use a different alignment function)
* align words separately
* test case: empty theme, compare output with input
"""

__author__ = "Marcel Martin"

import sys
import string
import cgi
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from . import ansicolor as ansi
from . import align


class HelpfulArgumentParser(ArgumentParser):
	"""An ArgumentParser that prints full help on errors."""
	def error(self, message):
		self.print_help(sys.stderr)
		args = {'prog': self.prog, 'message': message}
		self.exit(2, '%(prog)s: error: %(message)s\n' % args)


class Theme(object):
	def __init__(self, header, footer, lineend, minuscode, pluscode, delcode, inscode, modcode, atcode, minusescode, plusescode, specialcode, endcode, misc):
		self.header = header
		self.footer = footer
		self.lineend = lineend
		self.minuscode = minuscode
		self.pluscode = pluscode
		self.delcode = delcode
		self.inscode = inscode
		self.modcode = modcode
		self.atcode = atcode
		self.minusescode = minusescode
		self.plusescode = plusescode
		self.specialcode = specialcode
		self.endcode = endcode
		self.misc = misc

	def escape(self, text):
		return text


class HTMLTheme(Theme):
	def __init__(self):
		self.header = '<html><body><pre>\n' #div style="font-family:monospace">\n',
		self.footer = '\n</pre></body></html>\n'
		self.endcode = '</span>'
		self.lineend = '\n' #'<br/>\n'
		self.minuscode = '<span style="color:red">'
		self.pluscode = '<span style="color:green">'
		#self.minuscode = 'MINUS<span MINUS style="color:red">'
		#self.pluscode = 'PLUS<span PLUS style="color:green">'
		self.delcode = '<span style="color:black;background-color:#ffd0d0">'
		self.inscode = '<span style="color:black;background-color:#c0ffc0">' #"\x1b[30;42m", # black on green
		self.modcode = '<span style="color:black;background-color:lightgray">' #\x1b[30;47m", # black on gray
		self.atcode = '<span style="color:brown">'
		self.minusescode = '<span style="color:red">'
		self.plusescode = '<span style="color:green">'
		self.specialcode = '<span style="background-color:green">'
		self.misc = '<span style="color:gray">'

	def escape(self, text):
		return cgi.escape(text)


class NoColorTheme(Theme):
	def __init__(self):
		self.header = ''
		self.footer = ''
		self.lineend = '\n'
		self.minuscode = ansi.RED
		self.pluscode = ansi.GREEN
		self.delcode = ansi.BACKGROUND_RED
		self.inscode = "\x1b[4;42m" # "\x1b[30;42m", # white on green
		self.modcode = "\x1b[30;47m" # black on gray
		self.atcode = ansi.BROWN
		self.minusescode = ansi.RED
		self.plusescode = ansi.GREEN
		self.specialcode = ansi.BACKGROUND_GREEN
		self.endcode = ansi.RESET
		self.misc = ansi.GRAY


class DebugTheme(Theme):
	def __init__(self):
		self.header = 'HEADER'
		self.footer = 'FOOTER'
		self.lineend = 'LINEEND'
		self.minuscode = 'MINUS'
		self.pluscode = 'PLUS'
		self.delcode = 'DELETE'
		self.inscode = 'INSERT'
		self.modcode = 'MODIFY'
		self.atcode = 'AT'
		self.minusescode = 'MINUSES'
		self.plusescode = 'PLUSES'
		self.specialcode = 'SPECIAL'
		self.endcode = 'RESET'
		self.misc = 'MISC'


# pre-defined color themes
themes = [
	Theme(
		header = '',
		footer = '',
		lineend = '\n',
		minuscode = ansi.RED,
		pluscode = ansi.GREEN,
		delcode = ansi.BACKGROUND_RED,
		inscode = "\x1b[4;32m", # or white on green: "\x1b[97;42m"
		modcode = "\x1b[30;47m", # black on gray
		atcode = ansi.BROWN,
		minusescode = ansi.RED,
		plusescode = ansi.GREEN,
		specialcode = ansi.BACKGROUND_GREEN,
		endcode = ansi.RESET,
		misc = ansi.GRAY
		),

	HTMLTheme(),

	NoColorTheme(),

	DebugTheme(),

	Theme(
		header = '',
		footer = '',
		lineend = '\n',
		minuscode = ansi.RED,
		pluscode = ansi.BLUE,
		delcode = ansi.BACKGROUND_RED,
		inscode = ansi.BACKGROUND_GREEN,
		modcode = ansi.BACKGROUND_BLUE,
		atcode = ansi.BROWN,
		minusescode = ansi.RED,
		plusescode = ansi.BLUE,
		specialcode = ansi.BACKGROUND_GREEN,
		endcode = ansi.RESET,
		misc = ansi.GRAY
	),
]


def colorize(row1, row2, theme):
	"""makes two colorful strings. row1 and row2 contain the alignment"""
	events1 = []
	events2 = []
	NORMAL = 1
	DELETING = 2
	INSERTING = 3
	MODIFYING = 4
	SPECIAL = 5

	GAPCHAR = None

	# iterate over alignment columns, filling events1 and events2
	for c1, c2 in zip(row1, row2):
		if c1 == GAPCHAR:
			events2.append( (INSERTING, c2) )
		elif c2 == GAPCHAR:
			events1.append( (DELETING, c1) )
		elif c1 != c2:
			events1.append( (MODIFYING, c1) )
			events2.append( (MODIFYING, c2) )
		else:
			events1.append( (NORMAL, c1) )
			events2.append( (NORMAL, c2) )

	# convert to printable string
	codes = {
		INSERTING: theme.inscode,
		MODIFYING: theme.modcode,
		DELETING: theme.delcode,
		SPECIAL: theme.specialcode,
		}
	out = [None, None]
	eventlists = [events1, events2]
	normalcodes = [theme.minuscode, theme.pluscode]
	lines = [ [], [] ]
	chars = ['-', '+']
	for i in range(2):
		events = eventlists[i]
		normalcode = normalcodes[i]
		codes[NORMAL] = normalcode

		# start by changing color to 'normal' (plus/minus color)
		line = normalcode + chars[i]
		mode = NORMAL
		for event, c in events:
			# special case for line break within alignment
			if c == '\n':
				line += theme.endcode + theme.lineend
				lines[i].append(line)
				line = normalcode + chars[i]
				# enforce that code gets inserted for next char, needed because
				# the string will later be split at the '\n'
				mode = NORMAL
			elif mode != event:
				line += theme.endcode + codes[event]
				mode = event
				line += theme.escape(c)
			else:
				line += theme.escape(c)
	assert c == '\n'
	return lines


def tokenize(s):
	seq = []
	cur = None
	split_chars = string.digits + string.punctuation + string.whitespace
	for c in s:
		if c in split_chars:
			if not (cur is None):
				seq.append(cur)
				cur = None
			seq.append(c)
		else:
			if cur is None:
				cur = c
			else:
				cur += c
	if not (cur is None):
		seq.append(cur)
	return seq


def parse_unified_diff(stream):
	"""Reads in a unified diff from stream and yields tuples (type, data), which can be either:

	("+-", (minus_lines, plus_lines))
	('@', text)
	('+++', text)
	('---', text)
	(' ', text)
	('X', text)   anything else

	text, minus_lines and plus_lines contain a newline character in the end.
	text always contains the full line.
	minus_lines and plus_lines do not contain the inital + and - characters.
	"""
	minus_lines = ""
	plus_lines = ""
	# mode constants (state machine)
	READING_PLUSES = 1
	READING_MINUSES = 2
	# current mode
	mode = 0
	for line in stream:
		first_char = line[0]
		if first_char == '-' and not line.startswith('---'):
			assert mode != READING_PLUSES # no minus after plus
			minus_lines += line[1:]
			mode = READING_MINUSES
		elif first_char == '+' and not line.startswith('+++'):
			plus_lines += line[1:]
			mode = READING_PLUSES
		else:
			if mode != 0:
				yield ('+-', (minus_lines, plus_lines) )
				mode = 0
				plus_lines = ''
				minus_lines = ''
			if first_char == '@':
				yield ('@', line)
			elif line.startswith('---'):
				yield ('---', line)
			elif line.startswith('+++'):
				yield ('+++', line)
			elif line.startswith(' '):
				yield (' ', line)
			else:
				yield ('X', line)
	# don't forget the last lines
	if mode != 0:
		yield ('+-', (minus_lines, plus_lines) )


def format_unified_diff(stream, use_characters, memlimit, theme):
	"""Reads a stream formatted as a unified diff and uses the
	color theme to visualise it."""

	codes = { '---': theme.minusescode, '+++': theme.plusescode, '@': theme.atcode, 'X': theme.misc }
	sys.stdout.write(theme.header)
	for event, data in parse_unified_diff(stream):
		#print "event:", repr(event)
		#print "data:", repr(data)

		if event == '+-':
			minus_lines, plus_lines = data
			# print plus/minus group
			if use_characters:
				seq1, seq2 = minus_lines, plus_lines
			else:
				seq1 = tokenize(minus_lines)
				seq2 = tokenize(plus_lines)

			# prevent too high memory usage during alignment
			if len(seq1) * len(seq2) <= memlimit:
				row1, row2, errors = align.globalalign(seq1, seq2)
				lines1, lines2 = colorize(row1, row2, theme)
				for line in lines1:
					sys.stdout.write(line)
				for line in lines2:
					sys.stdout.write(line)
			else:
				for line in minus_lines.split('\n')[:-1]:
					sys.stdout.write('-' + theme.escape(line) + theme.lineend)
				for line in plus_lines.split('\n')[:-1]:
					sys.stdout.write('+' + theme.escape(line) + theme.lineend)
		else:
			line = theme.escape(data)
			line = line[:-1] # remove newline
			if event == ' ':
				start = ''
				end = ''
			else:
				start = codes[event]
				end = theme.endcode
			sys.stdout.write(start + line + end + theme.lineend)
	sys.stdout.write(theme.footer)


def main():
	parser = HelpfulArgumentParser(description=__doc__,
		formatter_class=RawDescriptionHelpFormatter)
	parser.add_argument("--characters", "-c", action="store_true", default=False,
		help="Compare individual characters, not words (default: compare words)")
	parser.add_argument("--html", action="store_true", default=False,
		help="HTML output (default: %(default)s)")
	parser.add_argument("--memory", type=int, default=1000*1000,
		help="Limit memory usage")
	args = parser.parse_args()

	theme = themes[1] if args.html else themes[0]
	format_unified_diff(sys.stdin, args.characters, args.memory, theme)


if __name__ == "__main__":
	main()
