from array import array

def globalalign(s1, s2):
	"""
	Compute and return a global alignment between strings s1 and s2 using unit costs.

	A tuple (r1, r2, cost) is returned. r1 and r2 are lists that represent the
	alignment rows. They contain None as gap characters.
	"""
	if type(s1) is list:
		s1 = [" "] + s1
	else:
		s1 = " " + s1
	if type(s2) is list:
		s2 = [" "] + s2
	else:
		s2 = " " + s2
	n = len(s1)
	m = len(s2)

	# constants for the DP matrix
	LEFT = 1
	TOP = 2
	DIAG = 3

	# DP matrix (two tables are faster than a single table or a numpy array)
	costs = [ array('h', (m)*[0]) for x in range(n) ]
	directions = [ array('h', (m)*[0]) for x in range(n) ]

	# init tables
	for i in range(n):
		costs[i][0] = i
		directions[i][0] = LEFT
	for j in range(1, m):
		costs[0][j] = j
		directions[0][j] = TOP

	# calculate alignment (using unit costs)
	for i in range(1, n):
		for j in range(1, m):
			bt = DIAG
			cost = costs[i-1][j-1] + (1 if (s1[i] != s2[j]) else 0)
			tmp = costs[i-1][j] + 1
			if tmp < cost:
				bt = LEFT
				cost = tmp
			tmp = costs[i][j-1] + 1
			if tmp < cost:
				bt = TOP
				cost = tmp
			costs[i][j] = cost
			directions[i][j] = bt

	# trace back through directions table (starting in bottom right corner)
	r1 = []
	r2 = []
	i = n-1
	j = m-1

	# build reversed sequences (undone afterwards)
	while i != 0 or j != 0:
		v, d = costs[i][j], directions[i][j]
		if d == DIAG:
			r1.append(s1[i])
			r2.append(s2[j])
			i -= 1
			j -= 1
		elif d == TOP:
			r1.append(None)
			r2.append(s2[j])
			j -= 1
		elif d == LEFT:
			r1.append(s1[i])
			r2.append(None)
			i -= 1

	# undo reverse
	r1.reverse()
	r2.reverse()
	return (r1, r2, costs[-1][-1])
